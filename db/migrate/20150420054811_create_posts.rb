class CreatePosts < ActiveRecord::Migration
  def up
    create_table :posts do |t|
      t.string :content
      t.integer :status ,default: "published"
      t.integer :user_id
      t.integer :total_up_vote ,default: 0
      t.integer :total_down_vote,default: 0
      t.integer :total_flag_count,default: 0
      t.integer :organization_id

      t.timestamps null: false
    end
  end

  def down
    drop_table :posts 
  end
end
