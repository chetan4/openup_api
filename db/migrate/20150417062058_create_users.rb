class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :encrypted_email
      t.string :encrypted_pin
      t.integer :organization_id
      t.string :token

      t.timestamps null: false
    end
  end
end
