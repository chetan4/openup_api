class CreateUserMoodHistories < ActiveRecord::Migration
  def change
    create_table :user_mood_histories do |t|
      t.integer :organization_id
      t.integer :user_id
      t.integer :mood

      t.timestamps null: false
    end
  end
end
