class Organization < ActiveRecord::Base
  has_many :users, dependent: :destroy
  has_many :user_mood_histories, dependent: :destroy
  has_many :posts, dependent: :destroy
end
