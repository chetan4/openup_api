class UserMoodHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :organization
  validates_presence_of :mood
  validates :mood, :numericality => { :greater_than => 0, :less_than_or_equal_to => 100 },if: Proc.new{|obj| obj.present?}
end
