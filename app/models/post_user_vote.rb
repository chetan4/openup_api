class PostUserVote < ActiveRecord::Base
  belongs_to :user
  belongs_to :post

  enum vote: [:upvote, :downvote, :flag]
  validates_uniqueness_of :user_id, :scope => :post_id

end
