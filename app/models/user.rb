class User < ActiveRecord::Base
  belongs_to :organization
  has_many :user_mood_histories, dependent: :destroy
  has_many :posts
  has_many :post_user_votes

  EMAIL_REGEX = /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/

  validates_presence_of :encrypted_email, message: "Email can't be blank."
  validates_uniqueness_of :encrypted_email,message: "Email has already been taken"

  def verified?
    self.token.present?
  end
end
