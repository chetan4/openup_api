class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :organization
  has_many :post_user_votes, dependent: :destroy

  validates_presence_of :content ,message: "Post cant be blank" 
  validates :content ,:length => {maximum: 140 , message: "Post can have only 140 characters"} 
  enum status: [:published, :deleted, :flagged]

  def post_data(user)
    [ 
      post_id: self.id ,
      content: self.content,
      upvote_count: self.total_up_vote,
      downvote_count: self.total_down_vote,
      user_upvoted: user_upvoted(user),
      user_downvoted: user_downvoted(user),
      user_flagged: user_flagged(user),
      created_at: self.created_at
    ]
  end

  def user_flagged(user)
    user_vote = PostUserVote.where(post_id: self.id , user_id: user.id).last
    user_vote.try(:flag?) ? true : false
  end

  def user_upvoted(user)
    user_vote = PostUserVote.where(post_id: self.id , user_id: user.id).last
    user_vote.try(:upvote?) ? true : false
  end

  def user_downvoted(user)
    user_vote = PostUserVote.where(post_id: self.id , user_id: user.id).last
    user_vote.try(:downvote?) ? true : false
  end
end
