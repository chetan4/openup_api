class HomeController < ApplicationController
  def about
  end

  def terms_of_services
  end

  def privacy
  end

  def community_standards
  end
end
