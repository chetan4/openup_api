class Api::V1::BaseController < ApplicationController
  rescue_from Exception, :with => :internal_server_error
  before_filter :authorize_user

  def authorize_user
    @user = User.find_by_token(params[:user_token].to_s)
    render json:{success: false ,error:{data: "Invalid User.", error_code: 1001}} unless @user.present?
  end

  private

  def internal_server_error
    render :json => {:data => nil, success: false ,error: {data: "Something went wrong.", error_code: 1003}}, :status => :internal_server_error
  end
end