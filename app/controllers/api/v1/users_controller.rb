class Api::V1::UsersController < Api::V1::BaseController
  skip_before_filter :authorize_user ,except: [:invite]
  before_filter :check_if_user_is_valid, only: [:verify]

  def create
    @user = UserService.new(params).create
    if @user.persisted?
      render json: {success: true,error: {}}
    else
      render json: {success: false,error: {data: @user.errors.messages.values.join(", "), error_code: 1002}}
    end
  end

  def verify
    @user = UserService.new(params).verify
    if @user.present?
      render json: {success: true,user_token: @user.token, error: {}}
    else
      render json: {success: false ,error: {data: "Invalid pin.", error_code: 1002}}
    end
  end

  def invite
    if UserService.new(params).email_valid? params[:email]
      ApplicationMailer.send_invite_mail(params[:email]).deliver
      render json: {success: true ,error: {}}
    else
      render json: {success: true ,error: {data: "Invalid Email.", error_code: 1002}}
    end
  end

  def check_if_user_is_valid
    user = UserService.new(params).find_user
    render json:{success: false ,error:{data: "Invalid User", error_code: 1001}} unless user.present?
  end

end