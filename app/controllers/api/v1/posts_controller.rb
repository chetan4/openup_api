class Api::V1::PostsController < Api::V1::BaseController

  before_filter :raise_post_not_found_error, only: [:upvote, :downvote, :flag]
  before_filter :check_user_organization, only: [:upvote, :downvote, :flag]
  
  def index
    page = params[:page] || 1
    posts = @user.organization.posts.order('created_at DESC').page(page).per(10)
    post_data = posts.map{|post| post.post_data(@user)}.flatten
    render json: {success: true ,error:{},page:{current_page: page ,total_page: posts.total_pages},feed: post_data}
  end

  def create
    post = @user.posts.new(content: params[:content])
    post.organization = @user.organization
    if post.save
      render json: {success: true ,feed: post.post_data(@user).first, error: {}}
    else
      render json: {success: false , error:{data: post.errors.messages.values.join(", "), error_code: 1002}}
    end
  end

  def upvote
    user_vote, vote_removed, post_data = PostVoteService.new(@user, "upvote", params).vote
    if vote_removed
      render json: {success: true,feed: post_data.first,error: {}}
    else
      if user_vote.upvote?
        render json: {success: true,feed: post_data.first,error: {}}
      else
        render json: {success: false,error: {data: user_vote.errors.full_messages.join(", "), error_code: 1002}}
      end
    end
  end

  def downvote
    user_vote, vote_removed, post_data = PostVoteService.new(@user, "downvote", params).vote
    if vote_removed
      render json: {success: true,feed: post_data.first,error: {}}
    else
      if user_vote.downvote?
        render json: {success: true,feed: post_data.first,error: {}}
      else
        render json: {success: false,error: {data: user_vote.errors.full_messages.join(", "), error_code: 1002}}
      end
    end
  end

  def flag
    user_vote, vote_removed, post_data = PostVoteService.new(@user, "flag", params).vote
    if vote_removed
      render json: {success: true,feed: post_data.first,error: {}}
    else
      if user_vote.flag?
        render json: {success: true,feed: post_data.first,error: {}}
      else
        render json: {success: false,error: {data: user_vote.errors.full_messages.join(", "), error_code: 1002}}
      end
    end
  end

  def check_user_organization
    @post = Post.find(params[:id])
    if @user.organization != @post.organization
      render json: {success: false,error: {data: "Invalid User.", error_code: 1004}}
    end
  end

  def raise_post_not_found_error
    post = Post.find_by_id(params[:id])
    render json: {success: false,error: {data: "Could not find post.", error_code: 1002}} unless post.present?
  end

end