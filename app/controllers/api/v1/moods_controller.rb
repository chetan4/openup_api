class Api::V1::MoodsController < Api::V1::BaseController

  def create
    mood = @user.user_mood_histories.new(mood: params[:mood])
    mood.organization = @user.organization
    if mood.save
      render :json => {success: true, error: {}}
    else
      render json: {success: false ,error: {data: mood.errors.messages.values.join(", "),error_code: 1002}}
    end
  end

end