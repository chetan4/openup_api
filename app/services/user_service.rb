class UserService
  attr_accessor :params

  def initialize(params)
    @params = params
  end

  def create
    user_pin = generate_pin
    user = find_user
    if user.present? && user.verified?
      user.update_attributes(encrypted_pin: encrypt(user_pin),token: nil)
    elsif user.present? && !user.verified?
      user.update_attributes(encrypted_pin: encrypt(user_pin))
    else
      user = User.new(encrypted_email: params[:email],encrypted_pin: encrypt(user_pin))
      return user unless validate_and_encrypt_email(user)
      if user.save
        user.organization = find_or_create_organization
        user.save
      end
    end
    send_verification_mail(params[:email],user_pin) unless user.errors.any?
    user
  end
  
  def verify
    user_token = generate_token
    user = User.where(encrypted_email: encrypt(params[:email]),encrypted_pin: encrypt(params[:pin])).last
    #dont update token if already verified
    user.update_attributes(token: user_token) if user.present? && !user.verified?
    user
  end

  def send_verification_mail(email,pin)
    ApplicationMailer.send_verification_pin(email,pin).deliver
  end

  def encrypt(content)
    Digest::SHA1.hexdigest(content.to_s)
  end

  def find_or_create_organization
    Organization.find_or_create_by(domain: params[:email].split("@").last)
  end

  def validate_and_encrypt_email(user)
    if user.encrypted_email.present?
      if email_valid? user.encrypted_email
        user.encrypted_email = Digest::SHA1.hexdigest(user.encrypted_email)
      else
        user.errors[:base] << "Email is invalid."
        return false
      end
    end
    true
  end

  def email_valid? email
    email.match(User::EMAIL_REGEX)
  end
  
  #generates 4 digit code
  def generate_pin
    rand.to_s[2..5]
  end

  def generate_token
    "user_#{SecureRandom.hex(10)}"
  end

  def find_user
    User.find_by_encrypted_email(encrypt(params[:email]))
  end

end