class PostVoteService

  attr_accessor :post, :post_user_vote, :user, :vote_type

  def initialize(user,vote_type, params)
    @user = user
    @vote_type = vote_type
    @post = Post.find(params[:id])
    @post_user_vote = PostUserVote.where(post_id: params[:id],user_id: user.id).last
  end

  def vote
    return [@post_user_vote, true, @post.post_data(@user)] if remove_vote
    if @post_user_vote.present?
      # decrement count
      update_post_vote_count(@post_user_vote.vote)
    end

    @post_user_vote ||= create_post_user_vote
    
    @post_user_vote.update_attributes(vote: vote_type)
    #increment count
    update_post_vote_count(vote_type, true)
    post_data = @post.post_data(@user)
    [@post_user_vote,false,post_data]
  end

  def remove_vote
    if(@post_user_vote.present? && (@post_user_vote.vote == vote_type))
      update_post_vote_count(@post_user_vote.vote)
      @post_user_vote.destroy
      true
    else
      false
    end
  end

  def update_post_vote_count(vote, increment = false)
    operator = increment ? "+" : "-"
    case vote
    when "upvote"
      update_vote_count(:total_up_vote,operator)
    when "downvote"
      update_vote_count(:total_down_vote,operator)
    when "flag"
      update_vote_count(:total_flag_count,operator)
    end
  end

  def update_vote_count(vote_count_column,operator)
    votes_count = post.send(vote_count_column).send(operator.to_sym,1)
    votes_count = 0 if votes_count < 0 && (operator == "-")
    post.update_attributes(vote_count_column => votes_count)
  end

  def create_post_user_vote
    @user.post_user_votes.create(post_id: @post.id, vote: vote_type)
  end
	
end