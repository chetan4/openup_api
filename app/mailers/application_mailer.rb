class ApplicationMailer < ActionMailer::Base
  default from: "admin@openup.com"

  def send_verification_pin(email ,user_pin)
    @user_pin = user_pin
    @email = email
    mail(:to => @email, :subject => "Verify your account on Open Up")
  end

  def send_invite_mail(email)
    @email = email
    mail(:to => @email, :subject => "You are invited to openup")
  end
end