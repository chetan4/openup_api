require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get terms_of_services" do
    get :terms_of_services
    assert_response :success
  end

  test "should get privacy" do
    get :privacy
    assert_response :success
  end

  test "should get community_standards" do
    get :community_standards
    assert_response :success
  end

end
