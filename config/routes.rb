Rails.application.routes.draw do

  get 'about' => "home#about"
  get 'terms_of_services' => "home#terms_of_services"
  get 'privacy' => "home#privacy"
  get 'community_standards' => "home#community_standards"

  namespace :api do
    namespace :v1 do      
      resources :users do
        collection do
          put :verify
          post :invite
        end
      end
      post "users/update_mood" => "moods#create"      
      resources :posts do
        member do
          post :downvote
          post :upvote
          post :flag
        end
      end
    end
  end
end
